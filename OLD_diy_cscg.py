#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Based on

Rikhye, R. V., Gothoskar, N., Guntupalli, J. S., Dedieu, A., Lázaro-Gredilla, M., & George, D. (2020). 
Learning cognitive maps as structured graphs for vicarious evaluation. 
BioRxiv, 864421. https://doi.org/10.1101/864421

Implements a clone-structured cognitive graph (cscg) capable of using 
different kinds of clustering methods for representing its observables.

by Lea Musiolek
"""


# TODO: Incorporate merging/pruning?
# TODO: Implement goal search and planning (Dijkstra's algorithm?)
# TODO: Implement community detection/hierarchy/generalization/rigidity
# TODO: Implement online learning with precision weighing


import numpy as np
import pandas as pd
import scipy.stats as st
from datetime import datetime as dt


class cscg_map():

    def __init__(self, map_type, cscg_memory, 
                 activation_threshold, 
                 learning_rate, 
                 scale, 
                 smoothing_divisor,
                 max_observables, 
                 max_hiddens, 
                 cloning_entropy, 
                 cloning_eminence):
        self.cscg_memory = cscg_memory # linear weight of old information vs. new
        self.activation_threshold = activation_threshold
        self.learning_rate = learning_rate
        self.max_observables = max_observables # maximal number of observable nodes
        self.max_hiddens = max_hiddens # maximal number of hidden nodes per observable
        self.scale = scale # standard deviation of Gaussian distribution for data points' distances from nodes, determines resolution of neural gas
        self.smoothing_divisor = smoothing_divisor
        self.cloning_entropy = cloning_entropy
        self.cloning_eminence = cloning_eminence
        if map_type == "neuralgas":
            from gwr_neuralgas import gwr_neuralgas
        
            
    def initialize_T_components(self): # Initialize numerator and denominator for computing T recursively
        self.T_denominator = self.prior_hidden_distribution.rename(index={'pi':'denom'})
        self.T_numerator = self.Transition_matrix.mul(self.T_denominator.values, axis=0)
         
    def load_model(self, continue_sequence: bool, direc, stamp):
        E=pd.read_csv(direc+"/Emissionmatrix_"+stamp+".csv", sep=";", index_col=0)
        T=pd.read_csv(direc+"/Transitionmatrix_"+stamp+".csv", sep=";", index_col=0)
        G=pd.read_csv(direc+"/Neuralgas_"+stamp+".csv", sep=";", index_col=0)
        pi=pd.read_csv(direc+"/priordist_"+stamp+".csv", sep=";", index_col=0)
        feature_max=pd.read_csv(direc+"/feature_max_"+stamp+".csv", sep=";", index_col=0, header=None)
        feature_min=pd.read_csv(direc+"/feature_min_"+stamp+".csv", sep=";", index_col=0, header=None)
        model_growth = pd.read_csv(direc+"/modelgrowth_"+stamp+".csv", sep=";", index_col=0)
        activation_log = pd.read_csv(direc+"/activation_log_"+stamp+".csv", sep=";", index_col=0)
        self.Neural_gas = G.copy() # Neural gas with observables as rows and their coordinates as columns
        self.Transition_matrix = T.copy() # quadratic data frame with transition matrix
        self.Emissions_matrix = E.copy() # Emission matrix
        self.prior_hidden_distribution = pi.copy() # Prior belief distribution
        self.feature_max = feature_max.copy()
        self.feature_min = feature_min.copy()
        self.model_growth = model_growth.copy()
        self.activation_log = activation_log.copy()
        self.conv = False
        if continue_sequence:
            T_denominator=pd.read_csv(direc+"/Denominator_"+stamp+".csv", sep=";", index_col=0)
            T_numerator=pd.read_csv(direc+"/Numerator_"+stamp+".csv", sep=";", index_col=0)
            current_belief=pd.read_csv(direc+"/currentbelief_"+stamp+".csv", sep=";", index_col=0)
            self.T_numerator = T_numerator.copy()
            self.T_denominator = T_denominator.copy()
            self.current_belief = current_belief.copy()
        else:
            self.initialize_T_components()
            self.current_belief = self.prior_hidden_distribution.copy()
        print('Loaded the CSCG - neural gas.')
    
    def new_model(self, startpoint):
        print('Initializing the CSCG - neural gas.')
        H_name = 'H'+str(0)+'_'+str(0) # Name of first hidden node
        O_name = 'O'+str(0) # Name of first observable node
        self.Neural_gas = pd.DataFrame(data=[startpoint], index =[O_name], columns=startpoint.index.to_list())
        self.Transition_matrix = pd.DataFrame(data=1.0, index =[H_name], columns =[H_name]) # Transition matrix
        self.Emissions_matrix = pd.DataFrame(data=1.0, index =[H_name], columns =[O_name]) # Emission matrix
        self.prior_hidden_distribution = pd.DataFrame(data=1.0, index = ['pi'], columns = [H_name]) # Prior
        self.initialize_T_components()
        self.current_belief = self.prior_hidden_distribution.copy()
        self.feature_max = pd.Series(data=np.nan, index=startpoint.index)
        self.feature_min = pd.Series(data=np.nan, index=startpoint.index)
        self.model_growth = pd.DataFrame(data=[[np.nan, len(self.Transition_matrix), len(self.Neural_gas)]], index=[0], columns=["data_point", "hidden_size","gas_size"])
        self.activation_log = pd.DataFrame(data=[[np.nan, 0]], index =[0], columns=["data_point", "activation_sum"])
        self.conv = False
         
    def save_model(self, direc, stamp, last_datapoint):
        self.Neural_gas.to_csv(direc+"/Neuralgas_"+stamp+".csv", sep=";")
        self.Transition_matrix.to_csv(direc+"/Transitionmatrix_"+stamp+".csv", sep=";")
        self.Emissions_matrix.to_csv(direc+"/Emissionmatrix_"+stamp+".csv", sep=";")
        self.prior_hidden_distribution.to_csv(direc+"/priordist_"+stamp+".csv", sep=";")
        self.T_numerator.to_csv(direc+"/Numerator_"+stamp+".csv", sep=";")
        self.T_denominator.to_csv(direc+"/Denominator_"+stamp+".csv", sep=";")
        self.current_belief.to_csv(direc+"/currentbelief_"+stamp+".csv", sep=";")
        self.feature_max.to_csv(direc+"/feature_max_"+stamp+".csv", sep=";")
        self.feature_min.to_csv(direc+"/feature_min_"+stamp+".csv", sep=";")
        
    def log_write(self, direc, stamp, last_datapoint, training_time, convergence_window):
        self.model_growth.to_csv(direc+"/modelgrowth_"+stamp+".csv", sep=";")
        self.activation_log.to_csv(direc+"/activation_log_"+stamp+".csv", sep=";")
        log = {"Neural_gas_nans":self.Neural_gas.isnull().sum().sum(), 
                "Transition_matrix_nans":self.Transition_matrix.isnull().sum().sum(), 
                "Emissions_matrix_nans":self.Emissions_matrix.isnull().sum().sum(), 
                "prior_hidden_distribution_nans":self.prior_hidden_distribution.isnull().sum().sum(), 
                "current_belief_nans":self.current_belief.isnull().sum().sum(), 
                "T_denominator_nans":self.T_denominator.isnull().sum().sum(), 
                "T_numerator_nans":self.T_numerator.isnull().sum().sum(),
                "last_datapoint":str(last_datapoint),
                "T_errors":str(self.T_errors),
                "converged":str(self.conv),
                "final_activation":self.activation_log["activation_sum"].tail(convergence_window).mean(),
                "training_time":str(training_time),
                "map_size":str(len(self.Neural_gas))
                "cscg_size":str(len(self.Transition_matrix))}
        df = pd.DataFrame(log, index=[0])
        df.to_csv(direc+"/log_"+stamp+".csv", sep=";")

                
    def new_observable(self, pos): # Create a new observable and one corresponding clone
        print('- Creating new observable at ' + str(pos))
        hidden_size = len(self.Transition_matrix)
        gas_size = len(self.Neural_gas)
        H_name = 'H'+str(gas_size)+'_'+str(0)
        
        self.Neural_gas.loc['O'+str(gas_size)] = pos
        self.T_denominator[H_name] = self.T_denominator.mean(axis=1) # expand denominator with average prior probability                
        self.T_numerator.loc[H_name,:] = self.T_denominator[H_name].values[0]/hidden_size # give new node uniform out-transitions              
        self.T_numerator[H_name] = self.T_numerator.mean(axis=1) # give new node average in-transitions
        self.T_denominator = self.T_denominator + self.T_numerator[H_name].values # increase denominator to ensure all rows of T will sum to 1
        self.Transition_matrix[H_name] = 1/hidden_size # expand transition matrix
        self.Transition_matrix.loc[H_name,:] = 1/hidden_size
        self.Transition_matrix = self.Transition_matrix.div(self.Transition_matrix.sum(axis=1), axis=0) # re-normalize T
        
        if sum(sum(np.round(self.Transition_matrix.values, 1) == np.round(self.T_numerator.div(self.T_denominator.values, axis=0).values, 1))) == len(self.Transition_matrix)**2:
            print("  - New hidden node '"+H_name+"' created successfully with new observable")
        else:
            message="  !ATTENTION! New transition matrix does not fit quotient after creating " +H_name+ " with new obs."
            print(message)
            self.T_errors += 1
        
        self.Emissions_matrix['O'+str(gas_size)] = 0 # expand emission matrix
        self.Emissions_matrix.loc[H_name,:] = 0
        self.Emissions_matrix.loc[H_name,'O'+str(gas_size)] = 1

        self.prior_hidden_distribution[H_name] = 1/hidden_size # expand pi
        self.prior_hidden_distribution = self.prior_hidden_distribution.div(self.prior_hidden_distribution.sum(axis=1), axis=0)
        self.current_belief[H_name] = 1/hidden_size # expand belief dist
        self.current_belief = self.current_belief.div(self.current_belief.sum(axis=1), axis=0)
        return H_name
        
        
    def new_clone(self, old: str): # Clone an existing clone with same out-transitions and average in-transitions
        hidden_size = len(self.Transition_matrix)
        root = old.split('_')[0]        
        clones = self.Transition_matrix.index.values.tolist()
        matching = [s for s in clones if root in s]
        if len(self.Transition_matrix) <= self.max_hiddens:
            print('- Creating new clone for ' + root)
            H_name = root+'_'+str(len(matching))            
            self.T_denominator[H_name] = self.T_denominator.mean(axis=1) # expand denominator with average prior probability                   
            old_out = self.T_numerator.loc[old,:]/sum(self.T_numerator.loc[old,:])
            self.T_numerator.loc[H_name,:] = old_out * self.T_denominator[H_name].values # normalize and transfer old out transitions to new node         
            self.T_numerator[H_name] = self.T_numerator.mean(axis=1) # give new node average in-transitions
            self.T_denominator = self.T_denominator + self.T_numerator[H_name].values # increase denominator to ensure all rows of T will sum to 1
            
            self.Transition_matrix.loc[H_name, :] = self.Transition_matrix.loc[old,:] # clone out-transitions between old and new nodes
            self.Transition_matrix[H_name] = 1/hidden_size # initialize new nodes with average in-transition probabilities from others
            self.Transition_matrix = self.Transition_matrix.div(self.Transition_matrix.sum(axis=1), axis=0) # re-normalize T
            
            if sum(sum(np.round(self.Transition_matrix.values, 1) == np.round(self.T_numerator.div(self.T_denominator.values, axis=0).values, 1))) == len(self.Transition_matrix)**2:
                print("  - New hidden node '"+H_name+"' cloned successfully")
            else:
                message="  !ATTENTION! New transition matrix does not fit quotient after cloning new " +H_name+"."
                print(message)
                self.T_errors += 1
            
            self.Emissions_matrix.loc[H_name, :] = 0 # expand emission matrix
            self.Emissions_matrix.loc[H_name, "O"+root.split("H")[1]] = 1

            self.prior_hidden_distribution[H_name] = 1/hidden_size # expand pi
            self.prior_hidden_distribution = self.prior_hidden_distribution.div(self.prior_hidden_distribution.sum(axis=1), axis=0)
            self.current_belief[H_name] = 1/hidden_size # expand belief dist
            self.current_belief = self.current_belief.div(self.current_belief.sum(axis=1), axis=0)
            return H_name
        else:
            print('  - Maximum number of hidden nodes reached, cannot create new clone!')
            return

    def get_point_likelihoods(self, x): # Get likelihoods of data point given the observables
        differences = self.Neural_gas.subtract(x, axis=1)
        assert sum(self.feature_max>0)==3, "Variable feature deviations not all above 0!"
        differences = differences.div((self.feature_max-self.feature_min), axis=1) # Feature scaling
        differences['dist'] = np.sqrt(np.square(differences).sum(axis=1))
        ## Compute likelihood of this or more distant data point given distributions around the closest nodes
        differences['likeli'] = abs(1 - st.norm(0, self.scale).cdf(differences.loc[:, 'dist'])) 
        differences = differences['likeli'].to_frame()
        differences = differences.reindex(self.Neural_gas.index)
        return differences
    
    def next_observable(self): # predict the next observable to appear and/or simulate it
        next_obs = self.current_belief @ self.Transition_matrix @ self.Emissions_matrix
        point_pred = next_obs.idxmax(axis=1)
        sample = np.random.choice(next_obs.index, p=next_obs.values)
        return point_pred, sample
       
    def cscg_inference(self, batch):
        ## Get likelihoods of observations given different observables
        indices = batch.index.to_list()
        likelihoods = self.get_point_likelihoods(batch.loc[indices[0],:])
        likelihoods.columns = ["obs"+str(0)]
        for i in range(1, len(batch)):
            curr = self.get_point_likelihoods(batch.loc[indices[i],:])
            curr.columns = ["obs"+str(i)]
            likelihoods = likelihoods.join(curr, how="outer")

        ## Compute alphas for this batch: the joint probabilities of seeing the observations O from 0 through n 
        # and being in hidden state i at time n given the model parameters
        # alpha_i(n) = p( O_(0:n), h_n=i | T, E, pi)
        alphas = np.zeros((len(batch), len(self.Transition_matrix)))
        alphas[0, :] = self.current_belief.values * (self.Emissions_matrix.values @ likelihoods["obs"+str(0)].values)
        for n in range(1, len(batch)):
            alphas[n, :] = (alphas[n - 1, :] @ self.Transition_matrix.values) * (self.Emissions_matrix.values @ likelihoods["obs"+str(n)].values)
        alphas = pd.DataFrame(alphas, columns = self.Transition_matrix.index.values, index = ["obs"+str(i) for i in range(0, len(batch))])

        ## Compute betas for this batch: the conditional probabilities of seeing the observations O from n+1 through 
        # to the end given state i at time n and the model parameters
        # beta_i(n) = p( O_(n+1:N) | h_n=i, T, E, pi)
        betas = np.zeros((len(batch), len(self.Transition_matrix)))
        betas[-1, :] = 1
        for n in range(len(batch) - 2, -1, -1):
            betas[n, :] = self.Transition_matrix.values @ ((self.Emissions_matrix.values @ likelihoods["obs"+str(n+1)].values) * betas[n+1, :])
        betas = pd.DataFrame(betas, columns = self.Transition_matrix.index, index = ["obs"+str(i) for i in range(0, len(batch))])

        ## Compute the joint distributions of seeing all observations O in the batch and being in hidden state i at 
        # time n
        # alphabetas_i(n) = p( O_(1:N), h_n=i | T, E, pi)
        alphabetas = alphas*betas

        ## Compute the final score: the probability of seeing this batch of observations given the model parameters
        # finalscore = p( O_(1:N) | T, E, pi)
        finalscore = sum(alphabetas.loc["obs"+str(len(batch)-1),:])
        print("Final score for this batch was "+str(finalscore))
        assert finalscore > 0, "Batch score underflow!"
        ## Compute the belief distribution given the batch
        self.current_belief = alphabetas.loc["obs"+str(len(batch)-1),:].to_frame()
        self.current_belief = self.current_belief.div(finalscore).T.rename(index={"obs"+str(len(batch)-1):'current_belief'})
        return likelihoods, alphas, betas, finalscore
        
    def cscg_train(self, batch):
        likelihoods, alphas, betas, finalscore = self.cscg_inference(batch)
        ## Compute gammas, the probabilities of being in state i at time n given the observations and the parameters
        # gammas_i(n) = p( h_n=i | O_(1:N), T, E, pi)
        gammas = (alphas*betas)
        assert sum(gammas.sum(axis=1)<=0)<=0, "Gammas <= 0 for at least one observation!"
        gammas = gammas.div(gammas.sum(axis=1), axis=0)       
        ## Summing over time gives gamma sums, the total probabilities of being in state i between time 1 and N-1
        # gammasums_i = p( h=i | O_(1:N), T, E, pi)
        gammasums = gammas[:-1].sum(axis=0).to_frame()
        gammasums = gammasums.rename(columns={0:'gamma_sums'})
        
        ## Compute digammas, the joint probabilities of being in state i at time n and in state j at time n+1
        # given the observations and the model parameters
        # digammas_ij(n) = p( h_n=i, h_(n+1)=j | O_(1:N), T, E, pi)
        digammas = np.zeros((len(batch), len(self.Transition_matrix), len(self.Transition_matrix)))
        for n in range(0, len(batch)-1):
            P1 = self.Transition_matrix.mul(alphas.loc["obs"+str(n), :], axis=0)
            P2 = (self.Emissions_matrix @ likelihoods["obs"+str(n+1)]) * betas.loc["obs"+str(n+1), :]            
            digammas[n, :, :] = P1.mul(P2, axis=1)
            assert sum(sum(digammas[n, :, :]))>0, "Digammas <= 0 for at least one observation!"
            digammas[n, :, :] = digammas[n, :, :]/sum(sum(digammas[n, :, :]))
        ## Summing over time gives digamma sums, the total probabilities of pairs of hidden states 
        # i and j occurring subsequently
        digammasums = pd.DataFrame(data=digammas.sum(axis=0), index=self.Transition_matrix.index, columns=self.Transition_matrix.index)
        ## Smoothing gammasums and digammasums to prevent division by 0
        smoother = np.mean(gammasums.values)/self.smoothing_divisor
        gammasums = gammasums + smoother*len(digammasums) 
        digammasums = digammasums + smoother
        self.gammasums = gammasums
        self.digammasums = digammasums
        self.right_ratios = sum(round(digammasums.div(gammasums.values, axis=0).sum(axis=1))==1)
        assert self.right_ratios > 0.9*len(digammasums), "Inference does not add up!"
    
        ## Normalizing digammasums by gamma sums gives the new matrix of transition probabilities
        self.T_numerator = self.cscg_memory * self.T_numerator + (1-self.cscg_memory) * digammasums
        self.T_denominator = self.cscg_memory * self.T_denominator + (1-self.cscg_memory) * gammasums.T.values
        assert sum(sum(self.T_denominator.values<=0)) == 0, "Denominator <= 0 for at least one state!!"
        self.Transition_matrix = self.T_numerator.div(self.T_denominator.values, axis=0)
        self.Transition_matrix = self.Transition_matrix.div(self.Transition_matrix.sum(axis=1), axis=0) # re-normalize T
        ## Computing a weighted sum of pi and the gammasums gives the updated pi
        self.prior_hidden_distribution = self.cscg_memory * self.prior_hidden_distribution + (1-self.cscg_memory) * gammasums.div(sum(gammasums.values)).T.values
        self.prior_hidden_distribution = self.prior_hidden_distribution.div(self.prior_hidden_distribution.sum(axis=1), axis=0)
        return
    
    def cloning_sweep(self):
        to_clone = list()
        criterion = self.cloning_eminence/len(self.Transition_matrix)
        for i in self.Transition_matrix.index:
            if self.prior_hidden_distribution[i].values > criterion and st.entropy(self.Transition_matrix.loc[i, :]) > self.cloning_entropy:
                to_clone.append(i)
        for H_name in to_clone:
            self.new_clone(H_name)
    
    def neuralgas_train(self, batch):
        provisory_belief = self.current_belief.copy() # The training of the neural gas does not update the model's belief
        for n in batch.index:
            print("- Data point "+str(n))
            bottom_up = self.get_point_likelihoods(batch.loc[n,:]).T
            top_down = provisory_belief @ self.Transition_matrix @ self.Emissions_matrix
            activation = bottom_up.mul(top_down.values, axis=1)
            activsum = sum(sum(activation.values))
            self.activation_log = self.activation_log.append(pd.DataFrame([[n, activsum]], columns=["data_point", "activation_sum"]))
            if bottom_up.values.max() < self.activation_threshold and len(self.Neural_gas) <= self.max_observables:
                H_name = self.new_observable(batch.loc[n,:].values)
                provisory_belief = pd.DataFrame(data=0.0, index = ['provis_belief'], columns = self.Transition_matrix.index)
                provisory_belief[H_name] = 1
            else:
                best_observable = bottom_up.T.idxmax()[0]
                print("Most active observable: "+str(best_observable))
                move = self.learning_rate * (batch.loc[n,:].values - self.Neural_gas.loc[best_observable,:].values)
                self.Neural_gas.loc[best_observable,:] += move
                print("- Moving observable " + best_observable + " to " + str(self.Neural_gas.loc[best_observable,:].values))
                provisory_belief = (provisory_belief @ self.Transition_matrix) * (self.Emissions_matrix @ bottom_up.T).T.values
                self.provisory_belief = provisory_belief
                if provisory_belief.sum(axis=1).sum() <= 0:
                    print("Current total belief at 0, stopping GWR-NG training on this batch.")
                    return
                assert sum(sum(provisory_belief.values)) > 0, "Summed provisory belief not > 0!!"
                provisory_belief = provisory_belief.div(provisory_belief.sum(axis=1), axis=0)
        return provisory_belief
    
    def feature_scaling(self, batch): # no standardizing because data is probably not normally distributed
        batchmax = np.max(batch, axis=0)
        batchmin = np.min(batch, axis=0)
        if sum(self.feature_max.isna())>0: # If there are no scales yet
            self.feature_max = batchmax
            self.feature_min = batchmin
        else:
            self.feature_max = pd.concat([batchmax, self.feature_max], axis=1).max(axis=1)
            self.feature_min = pd.concat([batchmin, self.feature_min], axis=1).min(axis=1)
     

    
    def full_train(self, filename, dat, batch_size, convergence_window, max_time, continue_sequence:bool, direc: str, stamp: str):        
        data_nans = dat.isnull().sum().sum()
        if data_nans > 0:            
            print("!ATTENTION! Data contains " + str(data_nans) + "NaN values.")        
        hypers = {"cscg_memory":str(self.cscg_memory),
                   "activation_threshold":str(self.activation_threshold),
                   "learning_rate":str(self.learning_rate), 
                   "scale":str(self.scale),
                   "smoothing_divisor":str(self.smoothing_divisor),
                   "max_observables":str(self.max_observables),
                   "max_hiddens":str(self.max_hiddens),
                   "cloning_entropy":str(self.cloning_entropy),
                   "cloning_eminence":str(self.cloning_eminence),
                   "batch_size":str(batch_size),
                   "convergence_window":str(convergence_window),
                   "continue_sequence":str(continue_sequence),
                   "max_time":str(max_time),
                   "data_file":filename,
                   "data_nans":str(data_nans)}       
        df2 = pd.DataFrame(hypers, index=[0])
        df2.to_csv(direc+"/hyper_parameters_"+stamp+".csv", sep=";")

        if not continue_sequence:
            self.current_belief = self.prior_hidden_distribution.copy()        
            
        self.starttime = dt.now()
        print("Starting training on data " + filename + ".\n")
        
        self.T_errors = 0
        i=min(dat.index.to_list())
        datend = max(dat.index.to_list())
        
        while i < datend:
            print("Starting training on new batch at data point "+str(i))
            if i+batch_size <= datend:
                batchend = i+batch_size
            else:
                batchend = datend
            self.feature_scaling(dat.loc[i:batchend,:])
            self.neuralgas_train(dat.loc[i:batchend,:])
            self.cscg_train(dat.loc[i:batchend,:])
            self.cloning_sweep()
            self.model_growth = self.model_growth.append(pd.DataFrame([[i, len(self.Transition_matrix), len(self.Neural_gas)]], columns=["data_point", "hidden_size","gas_size"]))
            self.save_model(direc, stamp, i)
            training_time = (dt.now()-self.starttime).total_seconds()
            self.log_write(direc, stamp, i, training_time, convergence_window)
            if self.conv or (training_time > max_time):
                break
            i=i+batch_size
        print("Finished training at data point "+str(batchend))
        return
