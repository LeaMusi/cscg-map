#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 15:18:16 2024

@author: spathiphyllum
"""
import sys
import glob
import os
import time

import pandas as pd
import numpy as np

import transforming_binning as tb
import chmm_wrappers as cw
import setup
sys.path.append(setup.fish_movements_path)

# Necessary to add cwd to path when script is run
# by SLURM (since it executes a copy)
# sys.path.append(os.getcwd())

# abspath = os.path.abspath(__file__)
# dname = os.path.dirname(abspath)
# os.chdir(dname)


#%% Directories, data import & transformation

downsample_factors = [1, 2, 3, 4, 5, 7, 10, 20]
spaces = ["whole", "dataset"]
max_number_segments = 8
max_epochs = 40
max_clones = 6
min_frames = 1000


#%% Preprocessing, training and testing



# with Pool(processes=num_proc) as pool:
#     pool.starmap(naosim.simulation_training,
#                   zip(alive, alive_genes, repeat(outfolder),
#                       repeat(PERFORMANCE_MEASURES), repeat(gui_bool),
#                       repeat(pair_bool), repeat(nao_posture),
#                       repeat(generation)))


counter = 0

for space in spaces:
    if space == "whole":
        dimensions = {"IID":{'min':0.0,'max':124.0},
        			  "heading_diff":{'min':-np.pi,'max':np.pi},
        			  "rel_pos":{'min':-np.pi,'max':np.pi}}
    elif space == "dataset":
        dimensions = {"IID":{'min':np.nan,'max':np.nan},
        			  "heading_diff":{'min':np.nan,'max':np.nan},
        			  "rel_pos":{'min':np.nan,'max':np.nan}}
    resolutions = [nseg**len(dimensions) for nseg in
                   range(2, max_number_segments+1)]
    for ds_factor in downsample_factors:
        for resolution in resolutions:
            rawfilepaths = glob.glob(setup.rawdata_direc
                              +"*")
            rawfilepaths.sort()
            for rawfilepath in rawfilepaths:
                filename = rawfilepath.split("/")[-1].split(".")[0]
                binfilepath = (setup.bindata_direc
                                +filename
                                +"_social_"
                                +space
                                +"space_ds_"
                                +str(ds_factor)
                                +"_"
                                +str(resolution)
                                +"bins.csv")

                # Transforming and binning if necessary
                if not os.path.exists(binfilepath):
                    if not os.path.exists(setup.socialdata_direc
                                                  +filename
                                                  +"_social.csv"):
                        tb.transform_social_all(
                            min_frames=min_frames)
                        tb.bin_file(filename=filename,
                                    min_frames=min_frames,
                                    downsample_factor=ds_factor,
                                    resolution=resolution,
                                    dimensions=dimensions,
                                    space=space,
                                    binfilepath=binfilepath)
                    else:
                        tb.bin_file(filename=filename,
                                    min_frames=min_frames,
                                    downsample_factor=ds_factor,
                                    resolution=resolution,
                                    dimensions=dimensions,
                                    space=space,
                                    binfilepath=binfilepath)
                if not os.path.exists(binfilepath): # Data not binnable
                    continue

                bindat = pd.read_csv(binfilepath, sep=",", header=0)
                binfilename = binfilepath.split("/")[-1].split(".")[0]

                # Training and testing
                halfdatlen = round(len(bindat)/2)
                half2 = bindat.loc[halfdatlen:, :].reset_index(drop=True)
                half1 = bindat.loc[0:halfdatlen-1, :].reset_index(drop=True)

                half2dat = half2["social_bin_trajectory"].values.astype(int)
                half1dat = half1["social_bin_trajectory"].values.astype(int)
                for clones_per_obs in range(1, max_clones+1):
                    counter += 1

                    if (os.path.exists(
                            setup.out_direc + "T_" + binfilename + "_"
                            + str(clones_per_obs) + "clones.csv")
                        and os.path.exists(
                            setup.out_direc + "Traininglog_" + binfilename + "_"
                            + str(clones_per_obs) + "clones.txt")):
                        print("Model already trained and saved")
                        continue
                    else:
                        log_path = cw.start_training_logfile(
                            dataname=binfilename,
                            direc=setup.out_direc,
                            chmm_config_str=str(clones_per_obs)+"clones")
                        print("Running with bin file "
                              + binfilename + " and "
                              + str(clones_per_obs) + " clones per observation.")

                        model = cw.traintest_chmm(
                            resolution=resolution,
                            clones_per_obs=clones_per_obs,
                            space=space,
                            max_epochs=max_epochs,
                            traindata=half2dat,
                            valdata=half1dat,
                            log_path=log_path)

                        cw.save_chmm(model=model,
                                      direc=setup.out_direc,
                                      dataname=binfilename,
                                      chmm_config_str=str(clones_per_obs)
                                                      +"clones")

print("Number of models trained and tested: " + str(counter))

all_transfiles = pd.DataFrame(columns=['filename', 'resolution', 'ds_factor',
                                       'space', 'transition_rate'])
transrates = glob.glob(setup.bindata_direc+"transitionrate*")
for transfile in transrates:
    trans = pd.read_csv(transfile, sep=",")
    all_transfiles = pd.concat([all_transfiles, trans], axis=0, join="outer")

all_transfiles.dropna(inplace=True)
all_transfiles.reset_index(inplace=True, drop=True)
all_transfiles.to_csv(setup.bindata_direc+"transition_rates.csv",
                       index=False,
                       sep=";")