#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on %(date)s

@author: %Lea Musiolek
"""

import sys
import glob
import os

import pandas as pd
import numpy as np
import tqdm
#import multiprocessing as mp

import setup
sys.path.append(setup.fish_movements_path)
import fish_movements as fm

#%%
def rawdata_to_socialspace(rawdata):
    assert np.shape(rawdata)[1] == 4, "Raw data does not have 4 columns!"
    assert np.sum(np.sum(rawdata.isna())) == 0, "Raw data contains NaN values!"

    dispx1, dispy1, magnit1, angle1 = fm.framewise_velocity(
        xseries=rawdata["x1"].values,
        yseries=rawdata["y1"].values)
    rawdata["dispx1"] = dispx1
    rawdata["dispy1"] = dispy1
    rawdata["magnit1"] = magnit1
    rawdata["angle1"] = angle1

    dispx2, dispy2, magnit2, angle2 = fm.framewise_velocity(
        xseries=rawdata["x2"].values,
        yseries=rawdata["y2"].values)
    rawdata["dispx2"] = dispx2
    rawdata["dispy2"] = dispy2
    rawdata["magnit2"] = magnit2
    rawdata["angle2"] = angle2


    rawdata["IID"] = np.nan
    rawdata["heading_diff"] = np.nan
    rawdata["rel_pos"] = np.nan
    for i in range(1, len(rawdata)):
        distance, fov_angle, heading_diff = fm.agent_partner_polar(
            agentx=rawdata["x1"][i],
            agenty=rawdata["y1"][i],
            agentang=rawdata["angle1"][i],
            partnerx=rawdata["x2"][i],
            partnery=rawdata["y2"][i],
            partnerang=rawdata["angle2"][i])
        rawdata["IID"][i] = distance
        rawdata["heading_diff"][i] = heading_diff
        rawdata["rel_pos"][i] = fov_angle

    socialdata = rawdata[["IID", "heading_diff", "rel_pos"]]
    socialdata = socialdata.dropna(axis=0)

    return socialdata

#%%
def data_binning(data:pd.core.frame.DataFrame, resolution:int,
                 dimensions):
    """Takes raw data of any dimensionality and bins it according to
    the chosen resolution."""

    num_segments = round(resolution**(1/len(dimensions)), 3)
    assert num_segments.is_integer(), ("Resolution**(1/number of dimensions)"
        + "is the number of segments per dimension and must be an integer!")
    cut_labels1 = [j for j in range(int(num_segments))]

    for dimension in dimensions:
        dimensions[dimension]['cut_bins'] = np.linspace(
            dimensions[dimension]['min'],
            dimensions[dimension]['max'],
            int(num_segments+1),
            endpoint=True)
        data[dimension+"_bins"] = pd.cut(
            data[dimension],
            bins=dimensions[dimension]['cut_bins'],
            labels=cut_labels1,
            include_lowest=True)

    data["social_bin_trajectory"] = (
        data["IID_bins"].astype(int) * num_segments**2 +
        data["heading_diff_bins"].astype(int) * num_segments
        + data["rel_pos_bins"].astype(int))
    cut_bins_IID  = dimensions["IID"]['cut_bins']
    cut_bins_headingdiff  = dimensions["heading_diff"]['cut_bins']
    cut_bins_relpos  = dimensions["rel_pos"]['cut_bins']
    for j in range(0, len(data)):
        data.loc[j,'IID_binmax'] = cut_bins_IID[
            data.loc[j, 'IID_bins']+1]
        data.loc[j,'heading_diff_binmax'] = cut_bins_headingdiff[
            data.loc[j, 'heading_diff_bins']+1]
        data.loc[j,'rel_pos_binmax'] = cut_bins_relpos[
            data.loc[j, 'rel_pos_bins']+1]
    data = data.dropna(axis=0)
    transitions = 0
    for j in range(0, len(data)-1):
        if (data.loc[j, "social_bin_trajectory"]
            != data.loc[j+1, "social_bin_trajectory"]):
            transitions += 1
    transition_rate = transitions/len(data)

    return data, transition_rate

#%%
def bin_file(filename:str, min_frames:int, downsample_factor:int,
             resolution:int, dimensions:dict, space:str, binfilepath:str):
    socfilepath = setup.socialdata_direc+filename+"_social.csv"

    transrates = pd.DataFrame(columns=['filename', 'resolution', 'ds_factor',
                                       'space', 'transition_rate'],
                              index=range(0,1))

    socialdata = pd.read_csv(socfilepath, sep=",", header=0)
    if len(socialdata)/downsample_factor < min_frames:
        print("Social dataset would contain too few "
              + "data points after downsampling!")
        return

    if space == "dataset":
        for dimension in dimensions:
            dimensions[dimension]['min'] = np.min(socialdata[dimension])
            dimensions[dimension]['max'] = np.max(socialdata[dimension])

    socialdata = socialdata.iloc[::downsample_factor, :]
    socialdata.reset_index(inplace=True, drop=True)

    bindata, transition_rate = data_binning(
        data=socialdata,
        resolution=resolution,
        dimensions=dimensions)
    transrates.loc[0,:] = [filename, resolution, downsample_factor, space,
                           transition_rate]
    bindata.to_csv(
        binfilepath,
        sep=",",
        index=False)
    binfile = binfilepath.split("/")[-1]
    transrates.to_csv(
        setup.bindata_direc
                         +"transitionrate_"
                         +binfile,
        sep=",",
        index=False)
    return

#%%
def transform_social_all(min_frames:int):
    rawfilepaths = glob.glob(setup.rawdata_direc+"*")
    for rawfilepath in tqdm.tqdm(rawfilepaths):
        filename = rawfilepath.split("/")[-1].split(".")[0]
        if os.path.exists(setup.socialdata_direc+filename+"_social.csv"):
            continue
        else:
            rawdata = pd.read_csv(rawfilepath, sep=";", header=None)

            # Deal with nans
            len_with_nans = len(rawdata)
            rawdata = rawdata.dropna(axis=0).reset_index(drop=True)
            len_no_nans = len(rawdata)
            print("Removed " + str(len_with_nans-len_no_nans)
                  + " rows with missing values from raw data")

            rawdata.columns=["x1", "y1", "x2", "y2"]
            socialdata = rawdata_to_socialspace(rawdata=rawdata)
            socialdata.to_csv(
                setup.socialdata_direc+filename+"_social.csv",
                index=False)