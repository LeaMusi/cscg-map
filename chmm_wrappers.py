#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: %Lea Musiolek
"""

import sys

import numpy as np
import pandas as pd

import setup
sys.path.append(setup.chmm_path)
import chmm_actions as ca


def test_chmm(model, testdata):
    """Compute the average prediction probability of a sequence of observations."""
    nll_per_prediction = model.bps(testdata, np.zeros(len(testdata), dtype=np.int64))
    pred_probs = 2**(-nll_per_prediction) # Reconverting the probs BEFORE averaging
    avg_prediction_probability = np.mean(pred_probs)
    print("Average chmm testing prediction probability: "+str(avg_prediction_probability))
    # What the authors do at
    # https://github.com/vicariousinc/naturecomm_cscg/blob/main/intro.ipynb
	# (underestimates the probs):
    #avg_nll = np.mean(nll_per_prediction)
    #avg_prediction_probability = 2**(-avg_nll)
    return avg_prediction_probability

def traintest_epoch_chmm(traindata, testdata, model):
    """Train and test a pre-defined model for one epoch."""
    model.learn_em_T_Pi(traindata, np.zeros(len(traindata), dtype=np.int64),
						n_iter=1, term_early=False)
    avpredprop = test_chmm(model, testdata)
    return model, avpredprop

def traintest_chmm(resolution, clones_per_obs, space, max_epochs, traindata,
                   valdata, log_path):
    """Create and train a chmm until the validation performance stops improving."""
    a_traindata = np.zeros(len(traindata), dtype=np.int64)
    n_clones = np.ones(resolution, dtype=np.int64) * clones_per_obs

    val_pi = np.histogram(valdata, bins=np.arange(resolution),
                          density=True)[0]
    train_pi = np.histogram(traindata, bins=np.arange(resolution),
                            density=True)[0]
    pi_avpredprob = np.dot(val_pi, train_pi)

    model = ca.CHMM(n_clones=n_clones, pseudocount=1e-10, x=traindata, a=a_traindata)
    logfile = open(log_path, "a")
    old_avpredprop = 0
    for epoch in range(0, max_epochs):
        model, avpredprop = traintest_epoch_chmm(traindata, valdata, model)
        logfile.write(
			"\n" + str(resolution) + ";"
			+ str(clones_per_obs) + ";"
            + space + ";"
			+ str(epoch) + ";"
            + str(avpredprop) + ";"
            + str(pi_avpredprob))
        if avpredprop <= round(old_avpredprop, 3):
            break
        else:
            old_avpredprop = (avpredprop + old_avpredprop)/2
    logfile.close()
    print("Stopped at epoch " + str(epoch) + " out of " + str(max_epochs))
    return model

def chmm_configuration_string(dataname, resolution, clones_per_obs):
	"""Create a string containing data name, resolution and clone number."""
	chmm_config_str = dataname + "__"+str(resolution)+"bins_"+str(clones_per_obs)+"clones"
	return chmm_config_str

def start_training_logfile(dataname, direc, chmm_config_str):
    """Create a log file for training and validating a chmm."""
    log_path = (direc + "/Traininglog_" + dataname + "_"
                + chmm_config_str + ".txt")
    logfile = open(log_path, "w")
    logfile.write("resolution;clones_per_obs;space;epoch;validation_avpredprop;"
                  +"pi_avpredprob")
    logfile.close()
    return log_path

def save_chmm(model, direc, dataname, chmm_config_str):
    """Save the parameters of a trained chmm."""
    Tdf = pd.DataFrame(model.T[0,:,:])
    Tdf.to_csv(direc + "/T_" + dataname + "_" + chmm_config_str + ".csv",
               sep=";",
               index=True)
    # pi = pd.DataFrame(model.Pi_x)
    # pi.to_csv(direc + "/Pi_" + dataname + "_" + chmm_config_str + ".csv",
    #           sep=";",
    #           index=True)