# Modular behavior model

This repository contains the central elements of a general animal behavior model with a module each for social behavior, non-social place preferences and physical movement constraints.
