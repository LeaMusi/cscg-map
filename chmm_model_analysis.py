#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %Lea Musiolek
"""

import os
import glob

import pandas as pd
import numpy as np
import pickle
import math
import igraph
import matplotlib
import igraph
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

import setup

outputselect = "Wholespace" # "Datasetspace" #

# Get model with highest POP
maxpop = pd.read_csv("".join([setup.out_direc,
                         outputselect,
                         "/All_",
                         outputselect,
                         "_bestPOPmodel.csv"]),
                sep=";",
                index_col=0,
                header=None)

filename = maxpop.loc["filename",1]
space = maxpop.loc["space",1]
ds_factor = maxpop.loc["ds_factor",1]
resolution = maxpop.loc["resolution",1]
clones_per_obs = maxpop.loc["clones_per_obs",1]

modelname = "".join([filename,
                     "_social_",
                     space,
                     "space_ds_",
                     ds_factor,
                     "_",
                     resolution,
                     "bins_",
                     clones_per_obs,
                     "clones"])

#%% Prune the transition matrix

Tdf = pd.read_csv("".join([setup.out_direc,
                         outputselect,
                         "/T_",
                         modelname,
                         ".csv"]),
                  sep=";",
                  index_col=0)

resolution = int(modelname.split("bins")[0].split("_")[-1])
num_clones = int(modelname.split("clones")[0].split("_")[-1])
chance_hidden = 1 / (resolution * num_clones)

# Prune T
Tdf_pruned = Tdf.copy()
Tdf_pruned[Tdf_pruned <= 0.1] = 0.0

#%% Identify semi-deterministic behavior sequences

sequences = [[[i], 1.0] for i in range(0,resolution * num_clones)]
longest_old = 1
longest = 2
s = 0
while s < len(sequences):
    sequence = sequences[s]
    goeson = False
    for i in range(0, resolution * num_clones):
        multip = sequence[1] * Tdf_pruned.iloc[sequence[0][-1],i]
        if multip > 0.5:
            goeson = True
            newseq = sequence[0].copy()
            newseq.append(i)
            sequences.append([newseq, multip])
            longest_old = longest
            longest = len(newseq)
    if goeson:
        sequences[s].append("obsolete")
    s += 1

transition_rates = []
for sequence in sequences:
    if not sequence[-1] == "obsolete":
        nodes = sequence[0].copy()
        observs = [node//num_clones for node in nodes]
        obs_transitions = 0
        lastobs = observs[0]
        for obs in observs:
            if obs != lastobs:
                obs_transitions += 1
                lastobs = obs
        sequence.append(obs_transitions)
        transition_rate = obs_transitions/len(sequence[0])
        sequence.append(transition_rate)
        transition_rates.append(transition_rate)
avg_transrate = np.mean(transition_rates)

filehandler = open("".join([setup.out_direc,
                            outputselect,
                            "/commonsequences_",
                            modelname,
                            ".obj"]),"wb")
pickle.dump(sequences, filehandler)
filehandler.close()

seqs = pd.DataFrame(columns=["model_name", "longest_sequence", "trans_rate"])
seqs.loc[0,:] = [modelname, longest, avg_transrate]
seqs.to_csv("".join([setup.out_direc,
                     outputselect,
                     "/Longest_sequence_",
                     outputselect,
                     ".csv"]))


#%% Visualize T in 3D

bindat = pd.read_csv(setup.bindata_direc
                             +filename
                             +"_social_"
                             +space
                             +"space_ds_"
                             +str(ds_factor)
                             +"_"
                             +str(resolution)
                             +"bins.csv",
                  sep=",")
points = bindat[["social_bin_trajectory",
                 "IID_binmax",
                 "heading_diff_binmax",
                 "rel_pos_binmax"]]
points = points.astype({
    'social_bin_trajectory': 'int',
    'IID_binmax': 'float',
    'heading_diff_binmax': 'float',
    'rel_pos_binmax': 'float'})
points.index = points["social_bin_trajectory"]
points = points[~points.index.duplicated(keep='first')]

Tdf_pruned = Tdf_pruned.stack()
Tdf_pruned = pd.DataFrame(Tdf_pruned)
Tdf_pruned["source"] = np.nan
Tdf_pruned["target"] = np.nan
for i in Tdf_pruned.index:
    if Tdf_pruned.loc[i,0] > 0.0:
        Tdf_pruned.loc[i, 'source'] = int(i[0])
        Tdf_pruned.loc[i, 'target'] = int(i[1])

Tdf_pruned.rename(columns={0: 'transprob'}, inplace=True)
Tdf_pruned.dropna(axis=0, inplace=True)
Tdf_pruned = Tdf_pruned[["source", "target", "transprob"]]
Tdf_pruned.reset_index(inplace=True, drop=True)

Tdf_pruned = Tdf_pruned.astype({
    'source': 'int',
    'target': 'int',
    'transprob': 'float'})

plt.close("all")
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim(0,124)
ax.set_ylim(-np.pi,np.pi)
ax.set_zlim(-np.pi,np.pi)

for i in Tdf_pruned.index:
    ax.plot(xs=[points.IID_binmax[Tdf_pruned.source[i]],
                  points.IID_binmax[Tdf_pruned.target[i]]],
              ys=[points.heading_diff_binmax[Tdf_pruned.source[i]],
                  points.heading_diff_binmax[Tdf_pruned.target[i]]],
              zs=[points.rel_pos_binmax[Tdf_pruned.source[i]],
                  points.rel_pos_binmax[Tdf_pruned.target[i]]])

fig.savefig(setup.out_direc
                  + outputselect
                  + "/Tdf_plot_"
                  + filename
                  + "_"
                  +space
                  +"space_ds_"
                  +str(ds_factor)
                  +"_"
                  +str(resolution)
                  +"bins_"
                  +str(clones_per_obs)
                  + "clones.png",
                  dpi=600)