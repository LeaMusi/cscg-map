#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on %(date)s

@author: %Lea Musiolek
"""

import sys
import pandas as pd
import glob
import os

import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
from scipy import stats
import seaborn as sns
import numpy as np
#import multiprocessing as mp

import setup

# Necessary to add cwd to path when script is run
# by SLURM (since it executes a copy)
# sys.path.append(os.getcwd())

# abspath = os.path.abspath(__file__)
# dname = os.path.dirname(abspath)
# os.chdir(dname)

outputselect = "Datasetspace" # "Wholespace" #

#%% Aggregate testing results

all_transrates = pd.read_csv(setup.bindata_direc+"transition_rates.csv",
                             sep=";")

logfiles = glob.glob(setup.out_direc+outputselect+"/Traininglog*"+"*")
all_logs = pd.DataFrame(columns=['filename', 'resolution', 'clones_per_obs',
                                 'space', 'ds_factor', 'epoch',
                                 'validation_avpredprop', 'pi_avpredprob'])
ix = 0
for logfile in logfiles:
    log = pd.read_csv(logfile, sep=";")
    filename = logfile.split("Traininglog_")[-1].split("_")
    log["filename"] = "_".join([filename[0], filename[1], filename[2]])
    ds_factor = logfile.split("Traininglog_")[-1].split("ds_")[-1].split("_")[0]
    log["ds_factor"] = int(ds_factor)

    all_logs.loc[ix,:] = log.loc[max(log.index),:]

    ix += 1
all_logs["prediction_over_prior"] = (all_logs.validation_avpredprop
                                 / all_logs.pi_avpredprob)

all_logs = pd.merge(all_logs,
                    all_transrates,
                    on=['filename', 'resolution', 'space', 'ds_factor'],
                    how="left")
all_logs = all_logs.astype({
    'prediction_over_prior': 'float',
    'resolution': 'int',
    'space': 'str',
    'ds_factor': 'int',
    'epoch': 'int',
    'validation_avpredprop': 'float',
    'pi_avpredprob': 'float',
    'clones_per_obs': 'int',
    'filename': 'category',
    'transition_rate': 'float'})

all_logs.to_csv("".join([setup.out_direc,
                         outputselect,
                         "/All_",
                         outputselect,
                         "_traininglogs.csv"]),
                sep=";",
                index=False)

maxpop = all_logs.iloc[all_logs["prediction_over_prior"].idxmax(),:]
maxpop.to_csv("".join([setup.out_direc,
                         outputselect,
                         "/All_",
                         outputselect,
                         "_bestPOPmodel.csv"]),
                sep=";",
                header=None)


#%% LMM functions

def lrt(mod1_logLike, mod2_logLike, mod1_df, mod2_df):
    '''
    Parameters
    ----------
    mod1_logLike : float
        Full model's ll
    mod2_logLike : float
        Simpler model's ll'
    mod1_df : float
        Full model's df'
    mod2_df : float
        Simpler model's df'

    Returns
    -------
    dict
        Results
    '''

    chi_square = 2 * (mod1_logLike - mod2_logLike)
    delta_params = abs(mod1_df - mod2_df)

    return {"chi_square" : chi_square,
            "df": delta_params,
            "p" : 1 - stats.chi2.cdf(chi_square, df=delta_params)}

def lmm_with_lrt(predvars:list, depvar:str, data, groups:str):

    # Full model
    full_md = smf.mixedlm(
        depvar + "~" + "+".join(predvars),
        data=data,
        groups=groups)
    full_mdf = full_md.fit(reml = False)
    full_mdf_results = full_mdf.summary().__dict__["tables"][0]
    full_mdf_results = np.append(full_mdf_results.iloc[:,0:2].values,
                                 full_mdf_results.loc[:,2:4].values,
                                 axis=0)
    full_mdf_results = pd.Series(full_mdf_results[:,1],
                                 index=full_mdf_results[:,0])
    full_mdf_coefs = full_mdf.summary().__dict__["tables"][1]
    full_mdf_coefs["Fixed effect"] = full_mdf_coefs.index
    full_df = (int(full_mdf_results["No. Observations:"]) -
               int(full_mdf_results["No. Groups:"]) -
               len(predvars))

    lrt_dataframe = pd.DataFrame(columns=["Fixed effect",
                                          "Chi squared",
                                          "Df",
                                          "Raw p",
                                          "BH-corrected p"],
                                 index=range(0,len(predvars)),
                                 dtype=float)

    # Partial models
    ix = 0
    for predvar in predvars:
        print(str(ix)+"\nPartial model for " + predvar)
        partial_predvars = predvars.copy()
        partial_predvars.remove(predvar)
        partial_md = smf.mixedlm(
            depvar + "~" + " + ".join(partial_predvars),
            data=data,
            groups=groups)

        partial_mdf = partial_md.fit(reml=False)
        partial_mdf_results = partial_mdf.summary().__dict__["tables"][0]
        partial_mdf_results = np.append(partial_mdf_results.iloc[:,0:2].values,
                                     partial_mdf_results.loc[:,2:4].values,
                                     axis=0)
        partial_mdf_results = pd.Series(partial_mdf_results[:,1],
                                     index=partial_mdf_results[:,0])
        partial_df = (int(partial_mdf_results["No. Observations:"]) -
                   int(partial_mdf_results["No. Groups:"]) -
                   len(partial_predvars))

        predvar_lrt = lrt(full_mdf.llf,
                       partial_mdf.llf,
                       full_df,
                       partial_df)

        lrt_dataframe.iloc[ix, :] = [predvar,
                                     float(predvar_lrt["chi_square"]),
                                     float(predvar_lrt["df"]),
                                     float(predvar_lrt["p"]),
                                     np.nan]

        ix += 1

    return lrt_dataframe, full_mdf_results, full_mdf_coefs

def probabilize(value:float):
    if value == 0.0:
        value += 0.00000001
    elif value == 1.0:
        value -= 0.00000001
    elif value < 0.0:
        return np.nan
    elif value > 1.0:
        return np.nan
    return value


#%% Analyses by resolution and clones:
# Perform linear mixed modeling on testing performance results

# Depvar: POP
(pop_lrt_dataframe, pop_full_mdf_results,
 pop_full_mdf_coefs) = lmm_with_lrt(
    predvars=['resolution', 'clones_per_obs', 'resolution:clones_per_obs'],
    depvar='prediction_over_prior',
    data=all_logs,
    groups='filename')
pop_old_ps = [probabilize(i) for i in
                     pop_lrt_dataframe["Raw p"].values]
pop_lrt_dataframe["BH-corrected p"] = stats.false_discovery_control(
    pop_old_ps,
    method="bh")
pop_lrt_dataframe.drop(columns=["Raw p"], inplace=True)
pop_lrt_dataframe = pop_lrt_dataframe.astype({
    'Df': 'int'})
pop_full_mdf_coefs.drop(index=["Intercept", "filename Var"],
                                 inplace=True)
pop_full_mdf_coefs = pop_full_mdf_coefs[
    ["Fixed effect", "Coef."]]
pop_lrt_dataframe = pd.merge(pop_lrt_dataframe,
         pop_full_mdf_coefs,
         on="Fixed effect")
pop_lrt_dataframe = pop_lrt_dataframe.astype({"Coef.":"float"})
pop_lrt_dataframe = pop_lrt_dataframe.round(2)
pop_lrt_dataframe.replace(
    to_replace="clones_per_obs",
    value="clones/obs",
    regex=True,
    inplace=True)
pop_lrt_dataframe.to_csv(
    setup.out_direc+outputselect+"/POP_"+outputselect+"_lmm_lrt.csv",
    index=False)

# Depvar: APP
(app_lrt_dataframe, app_full_mdf_results,
 app_full_mdf_coefs) = lmm_with_lrt(
    predvars=['resolution', 'clones_per_obs', 'resolution:clones_per_obs'],
    depvar='validation_avpredprop',
    data=all_logs,
    groups='filename')
app_old_ps = [probabilize(i) for i in
                     app_lrt_dataframe["Raw p"].values]
app_lrt_dataframe["BH-corrected p"] = stats.false_discovery_control(
    app_old_ps,
    method="bh")
app_lrt_dataframe.drop(columns=["Raw p"], inplace=True)
app_lrt_dataframe = app_lrt_dataframe.astype({
    'Df': 'int'})
app_full_mdf_coefs.drop(index=["Intercept", "filename Var"],
                                 inplace=True)
app_full_mdf_coefs = app_full_mdf_coefs[
    ["Fixed effect", "Coef."]]
app_lrt_dataframe = pd.merge(app_lrt_dataframe,
         app_full_mdf_coefs,
         on="Fixed effect")
app_lrt_dataframe = app_lrt_dataframe.astype({"Coef.":"float"})
app_lrt_dataframe = app_lrt_dataframe.round(2)
app_lrt_dataframe.replace(
    to_replace="clones_per_obs",
    value="clones/obs",
    regex=True,
    inplace=True)
app_lrt_dataframe.to_csv(
    setup.out_direc+outputselect+"/APP_"+outputselect+"_lmm_lrt.csv",
    index=False)

# Depvar: APRP
(aprp_lrt_dataframe, aprp_full_mdf_results,
 aprp_full_mdf_coefs) = lmm_with_lrt(
    predvars=['resolution', 'clones_per_obs', 'resolution:clones_per_obs'],
    depvar='pi_avpredprob',
    data=all_logs,
    groups='filename')
aprp_old_ps = [probabilize(i) for i in
                     aprp_lrt_dataframe["Raw p"].values]
aprp_lrt_dataframe["BH-corrected p"] = stats.false_discovery_control(
    aprp_old_ps,
    method="bh")
aprp_lrt_dataframe.drop(columns=["Raw p"], inplace=True)
aprp_lrt_dataframe = aprp_lrt_dataframe.astype({
    'Df': 'int'})
aprp_full_mdf_coefs.drop(index=["Intercept", "filename Var"],
                                 inplace=True)
aprp_full_mdf_coefs = aprp_full_mdf_coefs[
    ["Fixed effect", "Coef."]]
aprp_lrt_dataframe = pd.merge(aprp_lrt_dataframe,
         aprp_full_mdf_coefs,
         on="Fixed effect")
aprp_lrt_dataframe = aprp_lrt_dataframe.astype({"Coef.":"float"})
aprp_lrt_dataframe = aprp_lrt_dataframe.round(2)
aprp_lrt_dataframe.replace(
    to_replace="clones_per_obs",
    value="clones/obs",
    regex=True,
    inplace=True)
aprp_lrt_dataframe.to_csv(
    setup.out_direc+outputselect+"/APRP_"+outputselect+"_lmm_lrt.csv",
    index=False)

#%% Visualize testing results across datasets
# Visualize the effect of resolution on performance

ana_data = all_logs.loc[all_logs.groupby(['filename',
                                          'resolution',
                                          'clones_per_obs'])
                        ['prediction_over_prior'].idxmax()]

ana_data = ana_data.rename(columns={
    "filename": "Data set",
    "prediction_over_prior": "POP",
    "epoch": "Training epoch",
    "clones_per_obs": "Clones\nper\nobs",
    "resolution": "Resolution (voxels)",
    "validation_avpredprop": "APP",
    "pi_avpredprob": "APRP"})

sns.color_palette("rocket", as_cmap=True)
fig = sns.stripplot(data=ana_data,
                    x="Resolution (voxels)",
                    y="POP",
                    hue="Clones\nper\nobs",
                    jitter=True,
                    alpha=.6,
                    dodge=True,
                    palette="flare")
plt.legend(loc='upper left', ncol=1, prop={'size': 8}, title="Clones per obs")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_pop_"+outputselect
            +"_by_resolution.png", dpi=600)
plt.clf()

fig = sns.stripplot(data=ana_data,
                    x="Resolution (voxels)",
                    y="APP",
                    hue="Clones\nper\nobs",
                    jitter=True,
                    alpha=.6,
                    dodge=True,
                    palette="flare")
plt.legend(loc='upper left', ncol=8, prop={'size': 8}, title="Clones per obs")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_app_"+outputselect
            +"_by_resolution.png", dpi=600)
plt.clf()

fig = sns.stripplot(data=ana_data,
                    x="Resolution (voxels)",
                    y="APRP",
                    hue="Clones\nper\nobs",
                    jitter=True,
                    alpha=.6,
                    dodge=True,
                    palette="flare")
plt.legend(loc='upper left', ncol=8, prop={'size': 8}, title="Clones per obs")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_aprp_"+outputselect
            +"_by_resolution.png", dpi=600)
plt.clf()

#%% Visualize the effect of down-sampling on performance and transition rate

all_logs = all_logs.rename(columns={
    "filename": "Data set",
    "prediction_over_prior": "POP",
    "epoch": "Training epoch",
    "ds_factor": "Downsampling factor",
    "clones_per_obs": "Clones\nper\nobs",
    "resolution": "Resolution (voxels)",
    "validation_avpredprop": "APP",
    "pi_avpredprob": "APRP",
    "transition_rate": "Transition rate"})

fig = sns.lineplot(
    data=all_logs,
    x="Downsampling factor",
    y="APP",
    hue="Resolution (voxels)",
    err_style="bars",
    palette="crest")
plt.legend(loc='upper left', ncol=1, prop={'size': 8}, title="Resolution")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_app_"+outputselect
            +"_by_dsfactor.png", dpi=600)
plt.clf()

fig = sns.lineplot(
    data=all_logs,
    x="Downsampling factor",
    y="POP",
    hue="Resolution (voxels)",
    err_style="bars",
    palette="crest")
plt.legend(loc='upper left', ncol=1, prop={'size': 8}, title="Resolution")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_pop_"+outputselect
            +"_by_dsfactor.png", dpi=600)
plt.clf()

fig = sns.lineplot(
    data=all_logs,
    x="Downsampling factor",
    y="Transition rate",
    hue="Resolution (voxels)",
    err_style="bars",
    palette="crest")
plt.legend(loc='upper left', ncol=1, prop={'size': 8}, title="Resolution")
fig = fig.get_figure()
fig.savefig(setup.out_direc+outputselect+"/All_transrate_"+outputselect
            +"_by_dsfactor.png", dpi=600)
plt.clf()