#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 16:18:35 2024

@author: spathiphyllum
"""

runlocation = "local" # "remote" #

if runlocation == "local":
    fish_movements_path = "../Fish_movements"
    chmm_path = "/home/spathiphyllum/Desktop/Git/cscg_fork/"
    out_direc = "/home/spathiphyllum/Desktop/ScIoI/2021_CSCG_modeling/Output/"
    rawdata_direc = ("/home/spathiphyllum/Desktop/ScIoI/2021_CSCG_modeling/"
                  +"Extracted_fish_data/")
    socialdata_direc = ("/home/spathiphyllum/Desktop/ScIoI/2021_CSCG_modeling/"
                    + "Socialspace_data/")
    bindata_direc = ("/home/spathiphyllum/Desktop/ScIoI/2021_CSCG_modeling/"
                    + "Binned_data/")

elif runlocation == "remote":
    fish_movements_path = "/vol/home-vol3/koro/musiolel/Fish_movements/"
    chmm_path = "/vol/home-vol3/koro/musiolel/cscg_fork/"
    out_direc = "/vol/home-vol3/koro/musiolel/Output/"
    rawdata_direc ="/vol/home-vol3/koro/musiolel/Extracted_fish_data/"
    socialdata_direc = "/vol/home-vol3/koro/musiolel/Socialspace_data/"
    bindata_direc = "/vol/home-vol3/koro/musiolel/Binned_data/"